This is a Song API that you can CRUD songs which are is in-memory database.
To run this API, open the .sln file using Visual Studio and Run the solution. 

The MIT license gives users express permission to reuse code for any purpose, sometimes even if code is part of proprietary software. As long as users include the original copy of the MIT license in their distribution, they can make any changes or modifications to the code to suit their own needs.