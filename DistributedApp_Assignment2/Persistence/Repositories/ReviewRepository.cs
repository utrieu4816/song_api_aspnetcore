﻿using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Repositories;
using DistributedApp_Assignment2.Domain.Services.Communication;
using DistributedApp_Assignment2.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Persistence.Repositories
{
    public class ReviewRepository : BaseRepository, IReviewRepository
    {
        public ReviewRepository(AppDbContext context) : base(context) { }

        public async Task AddAsync(Review review)
        {
            await _context.Reviews.AddAsync(review);
        }

        public async Task<Review> FindByIdAsync(int id)
        {
            return await _context.Reviews.FindAsync(id);
        }

        public async Task<IEnumerable<Review>> ListAsync()
        {
            return await _context.Reviews.Include(r => r.Song).ToListAsync();
        }

        public void Remove(Review review)
        {
            _context.Reviews.Remove(review);
        }

        public void Update(Review review)
        {
            _context.Reviews.Update(review);
        }
    }
}
