﻿using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Repositories;
using DistributedApp_Assignment2.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Persistence.Repositories
{
    public class SongRepository : BaseRepository, ISongRepository
    {
        public SongRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Song>> ListAsync()
        {
            return await _context.Songs.ToListAsync();
        }

        public async Task AddAsync(Song song)
        {
            await _context.Songs.AddAsync(song);
        }

        public async Task<Song> FindByIdAsync(int id)
        {
            return await _context.Songs.FindAsync(id);
        }

        public void Update(Song song)
        {
            _context.Songs.Update(song);
        }

        public void Remove(Song song)
        {
            _context.Songs.Remove(song);
        }

        public async Task<IEnumerable<Song>> ListPopularSongsAsync()
        {
            return await _context.Songs.Where(s => s.IsPopular).ToListAsync();
        }
    }
}
