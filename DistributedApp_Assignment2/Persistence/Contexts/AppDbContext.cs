﻿using DistributedApp_Assignment2.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Persistence.Contexts
{
    public class AppDbContext : DbContext
    {
        public DbSet<Song> Songs { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Song>().ToTable("Songs");
            builder.Entity<Song>().HasKey(s => s.SongId);
            builder.Entity<Song>().Property(s => s.SongId).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Song>().Property(s => s.SongName).IsRequired();
            builder.Entity<Song>().Property(s => s.IsPopular).HasDefaultValue(false);
            builder.Entity<Song>().Property(s => s.FilePath).IsRequired();
            builder.Entity<Song>().HasMany(s => s.Reviews).WithOne(s => s.Song).HasForeignKey(s => s.SongId);

            builder.Entity<Song>().HasData
            (
                new Song { SongId = 100, SongName = "Hello", FilePath = "/songs/hello.mp3" }, // Id set manually due to in-memory provider
                new Song { SongId = 101, SongName = "Lemonade", IsPopular = true, FilePath = "/songs/lemonade.mp3" }
            );

            builder.Entity<Review>().ToTable("Reviews");
            builder.Entity<Review>().HasKey(r => r.ReviewId);
            builder.Entity<Review>().Property(r => r.ReviewId).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Review>().Property(r => r.ReviewContent).IsRequired();

            builder.Entity<Review>().HasData
            (
                new { ReviewId = 100, ReviewContent = "Good song!", SongId = 100 },
                new { ReviewId = 101, ReviewContent = "Love it!", SongId = 101 }
            );
        }
    }
}
