﻿using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Repositories;
using DistributedApp_Assignment2.Domain.Services;
using DistributedApp_Assignment2.Domain.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly ISongRepository _songRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IReviewRepository reviewRepository, ISongRepository songRepository, IUnitOfWork unitOfWork)
        {
            _reviewRepository = reviewRepository;
            _songRepository = songRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ReviewResponse> DeleteAsync(int id)
        {
            var existingReview = await _reviewRepository.FindByIdAsync(id);
            if (existingReview == null)
            {
                return new ReviewResponse("Review doesnnot found");
            }

            var song = await _songRepository.FindByIdAsync(existingReview.SongId);
            if (song == null)
            {
                return new ReviewResponse("Song does not found");
            }
            existingReview.Song = song;

            try
            {
                _reviewRepository.Remove(existingReview);
                await _unitOfWork.CompleteAsync();

                return new ReviewResponse(existingReview);
            }
            catch (Exception ex)
            {
                return new ReviewResponse($"An error occured when updating the song: {ex.Message}");
            }
        }

        public async Task<IEnumerable<Review>> ListAsync()
        {
            return await _reviewRepository.ListAsync();
        }

        public async Task<ReviewResponse> SaveAsync(Review review)
        {
            var song = await _songRepository.FindByIdAsync(review.SongId);
            if (song == null)
            {
                return new ReviewResponse($"Song does not exist");
            }
            try
            {
                review.Song = song;
                await _reviewRepository.AddAsync(review);
                await _unitOfWork.CompleteAsync();

                return new ReviewResponse(review);
            }
            catch (Exception ex)
            {
                return new ReviewResponse($"An error occured when creating the song: {ex.Message}");
            }
        }

        public async Task<ReviewResponse> UpdateAsync(int id, Review review)
        {
            var existingReview = await _reviewRepository.FindByIdAsync(id);
            if (existingReview == null)
            {
                return new ReviewResponse("Review does not found");
            }

            var song = await _songRepository.FindByIdAsync(existingReview.SongId);
            if (song == null)
            {
                return new ReviewResponse("Song does not found");
            }

            existingReview.ReviewContent = review.ReviewContent;
            existingReview.Song = song;
            

            try
            {
                _reviewRepository.Update(existingReview);
                await _unitOfWork.CompleteAsync();

                return new ReviewResponse(existingReview);
            }
            catch (Exception ex)
            {
                return new ReviewResponse($"An error occured when updating the song: {ex.Message}");
            }
        }


    }
}
