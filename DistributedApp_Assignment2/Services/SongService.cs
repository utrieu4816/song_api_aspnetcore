﻿using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Repositories;
using DistributedApp_Assignment2.Domain.Services;
using DistributedApp_Assignment2.Domain.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Services
{
    public class SongService : ISongService
    {
        private readonly ISongRepository _songRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SongService(ISongRepository songRepository, IUnitOfWork unitOfWork)
        {
            _songRepository = songRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Song>> ListAsync()
        {
            return await _songRepository.ListAsync();
        }

        public async Task<SongResponse> SaveAsync(Song song)
        {
            try
            {
                await _songRepository.AddAsync(song);
                await _unitOfWork.CompleteAsync();

                return new SongResponse(song);
            }
            catch (Exception ex)
            {
                return new SongResponse($"An error occurred when saving the song: {ex.Message}");
            }
        }

        public async Task<SongResponse> UpdateAsync(int id, Song song)
        {
            var existingSong = await _songRepository.FindByIdAsync(id);

            if (existingSong == null)
            {
                return new SongResponse("Song not found");
            }

            existingSong.SongName = song.SongName;
            existingSong.FilePath = song.FilePath;

            try
            {
                _songRepository.Update(existingSong);
                await _unitOfWork.CompleteAsync();

                return new SongResponse(existingSong);
            }
            catch (Exception ex)
            {
                return new SongResponse($"An error occured when updating the song: {ex.Message}");
            }
        }

        public async Task<SongResponse> DeleteAsync(int id)
        {
            var existingSong = await _songRepository.FindByIdAsync(id);

            if (existingSong == null)
            {
                return new SongResponse("Song not found");
            }


            try
            {
                _songRepository.Remove(existingSong);
                await _unitOfWork.CompleteAsync();

                return new SongResponse(existingSong);
            }
            catch (Exception ex)
            {
                return new SongResponse($"An error occured when updating the song: {ex.Message}");
            }
        }

        public async Task<SongResponse> GetSongById(int id)
        {
            var existingSong = await _songRepository.FindByIdAsync(id);

            if (existingSong == null)
            {
                return new SongResponse("Song not found");
            }

            return new SongResponse(existingSong);
        }

        public async Task<IEnumerable<Song>> ListPopularSongsAsync()
        {
            return await _songRepository.ListPopularSongsAsync();
        }
    }
}
