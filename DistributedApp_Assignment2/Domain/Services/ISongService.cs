﻿using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Domain.Services
{
    public interface ISongService
    {
        Task<IEnumerable<Song>> ListAsync();
        Task<SongResponse> SaveAsync(Song song);
        Task<SongResponse> UpdateAsync(int id, Song song);
        Task<SongResponse> DeleteAsync(int id);
        Task<SongResponse> GetSongById(int id);
        Task<IEnumerable<Song>> ListPopularSongsAsync();
    }
}
