﻿using DistributedApp_Assignment2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Domain.Services.Communication
{
    public class SongResponse : BaseResponse
    {
        public Song Song { get; private set; }

        private SongResponse(bool success, string message, Song song): base(success, message)
        {
            Song = song;
        }

        /// <summary>
        /// Create successful response
        /// </summary>
        /// <param name="song"></param>
        public SongResponse(Song song) : this(true, string.Empty, song) { }

        /// <summary>
        /// Create an error response
        /// </summary>
        /// <param name="messsage"></param>
        public SongResponse(string message) : this(false, message, null) { }
    }
}
