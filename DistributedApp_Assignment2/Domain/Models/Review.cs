﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Domain.Models
{
    public class Review
    {
        public int ReviewId { get; set; }
        public string ReviewContent { get; set; }

        public int SongId { get; set; }
        public Song Song { get; set; }
    }
}
