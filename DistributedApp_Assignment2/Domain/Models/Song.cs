﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Domain.Models
{
    public class Song
    {
        public int SongId { get; set; }
        public string SongName { get; set; }
        public bool IsPopular { get; set; }
        public string FilePath { get; set; }
        public IList<Review> Reviews { get; set; } = new List<Review>();
    }
}
