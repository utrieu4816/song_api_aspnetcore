﻿using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Domain.Repositories
{
    public interface IReviewRepository
    {
        Task<IEnumerable<Review>> ListAsync();
        Task AddAsync(Review review);
        Task<Review> FindByIdAsync(int id);
        void Update(Review review);
        void Remove(Review review);

    }
}
