﻿using DistributedApp_Assignment2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Domain.Repositories
{
    public interface ISongRepository
    {
        Task<IEnumerable<Song>> ListAsync();
        Task AddAsync(Song song);
        Task<Song> FindByIdAsync(int id);
        void Update(Song song);
        void Remove(Song song);
        Task<IEnumerable<Song>> ListPopularSongsAsync();
    }
}
