﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Services;
using DistributedApp_Assignment2.Extensions;
using DistributedApp_Assignment2.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DistributedApp_Assignment2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SongsController : Controller
    {
        private readonly ISongService _songService;
        private readonly IMapper _mapper;

        public SongsController(ISongService songService, IMapper mapper)
        {
            _songService = songService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<SongResource>> GetSongsAsync(bool isPopular)
        {
            IEnumerable<Song> songs;
            if (isPopular)
            {
                songs = await _songService.ListPopularSongsAsync();
            }
            else
            {
                songs = await _songService.ListAsync();
            }
            var resources = _mapper.Map<IEnumerable<Song>, IEnumerable<SongResource>>(songs);

            return resources;
        }

        [HttpGet("{id}")]
        public async Task<SongResource> GetSongById(int id)
        {
            var song = await _songService.GetSongById(id);
            var resource = _mapper.Map<Song, SongResource>(song.Song);

            return resource;
        }
        
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveSongResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var song = _mapper.Map<SaveSongResource, Song>(resource);
            var result = await _songService.SaveAsync(song);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var songResource = _mapper.Map<Song, SongResource>(result.Song);
            return Ok(songResource);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] SaveSongResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var song = _mapper.Map<SaveSongResource, Song>(resource);
            var result = await _songService.UpdateAsync(id, song);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var songResource = _mapper.Map<Song, SongResource>(result.Song);
            return Ok(songResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _songService.DeleteAsync(id);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var songResource = _mapper.Map<Song, SongResource>(result.Song);
            return Ok(songResource);
        }
    }
}
