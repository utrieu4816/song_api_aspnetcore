﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Domain.Services;
using DistributedApp_Assignment2.Extensions;
using DistributedApp_Assignment2.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DistributedApp_Assignment2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _reviewService;
        private readonly IMapper _mapper;

        public ReviewsController(IReviewService reviewService, IMapper mapper)
        {
            _reviewService = reviewService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<ReviewResource>> GetReviewsAsync()
        {
            var reviews = await _reviewService.ListAsync();
            var reviewResources = _mapper.Map<IEnumerable<Review>, IEnumerable<ReviewResource>>(reviews);

            return reviewResources;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveReviewResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var review = _mapper.Map<SaveReviewResource, Review>(resource);
            var result = await _reviewService.SaveAsync(review);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var reviewResource = _mapper.Map<Review, ReviewResource>(result.Review);
            return Ok(reviewResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] UpdateReviewResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var review = _mapper.Map<UpdateReviewResource, Review>(resource);
            var result = await _reviewService.UpdateAsync(id, review);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var reviewResource = _mapper.Map<Review, ReviewResource>(result.Review);
            return Ok(reviewResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _reviewService.DeleteAsync(id);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var reviewResource = _mapper.Map<Review, ReviewResource>(result.Review);
            return Ok(reviewResource);
        }
    }
}
