﻿using AutoMapper;
using DistributedApp_Assignment2.Domain.Models;
using DistributedApp_Assignment2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Song, SongResource>();
            CreateMap<Review, ReviewResource>();
        }
    }
}
