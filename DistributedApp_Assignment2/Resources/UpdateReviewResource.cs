﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Resources
{
    public class UpdateReviewResource
    {
        [Required]
        public string ReviewContent { get; set; }
    }
}
