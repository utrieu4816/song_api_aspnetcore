﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Resources
{
    public class SaveSongResource
    {
        [Required]
        public string SongName { get; set; }

        public string FilePath { get; set; }

        public string IsPopular { get; set; }
    }
}
