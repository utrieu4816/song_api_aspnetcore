﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Resources
{
    public class SongResource
    {
        public int SongId { get; set; }
        public string SongName { get; set; }
        public string FilePath { get; set; }

        public bool IsPopular { get; set; }
    }
}
