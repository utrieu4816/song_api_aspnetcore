﻿using DistributedApp_Assignment2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedApp_Assignment2.Resources
{
    public class ReviewResource
    {
        public int ReviewId { get; set; }
        public string ReviewContent { get; set; }

        public SongResource Song { get; set; }
    }
}
